FROM openjdk:11-jre
LABEL maintainer="Jaeho Lee <jhlee21071@gmail.com>"

WORKDIR /usr/src/myapp
COPY dockerize/entrypoint.sh /usr/src/myapp/
RUN chmod +x \
  /usr/src/myapp/entrypoint.sh

COPY target/*.jar /usr/src/myapp/backend.jar

ENTRYPOINT ["./entrypoint.sh"]